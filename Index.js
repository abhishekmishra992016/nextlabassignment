const enteredName = document.getElementById('Name');
const enteredMail = document.getElementById('Email');
const enteredComments = document.getElementById('Comments');
const onFormHandler = document.getElementById('formBtn');
const modelButtons = document.querySelectorAll('.btn-primary')

const sliderValue = document.getElementById('slider')
const sliderInput = document.getElementById('userSlider')
const priceTemplate1 = document.getElementById('priceTemplate1')
const priceTemplate2 = document.getElementById('priceTemplate2')
const priceTemplate3 = document.getElementById('priceTemplate3')


const loading = document.querySelector('.loading');




//    model Fade up
for (let i = 0; i < modelButtons.length; i++) {
    modelButtons[i].addEventListener('click', function () {
        $('#myModal').modal('show');
    })
}

function changeHighledElement(element) {
    element.style.backgroundColor = 'white'
}

// slider input 
sliderInput.addEventListener('input', () => {
    let value = sliderInput.value;
    sliderValue.innerText = value;

    if (value >= 0 && value <= 10) {
        priceTemplate1.style.backgroundColor = 'green'
        changeHighledElement(priceTemplate2)
    }
    else if (value >= 11 && value <= 20) {
        changeHighledElement(priceTemplate3);
        priceTemplate2.style.backgroundColor = 'orange'
        changeHighledElement(priceTemplate1)
    }
    else {
        changeHighledElement(priceTemplate2);
        priceTemplate3.style.backgroundColor = 'yellow'
        changeHighledElement(priceTemplate2);
    }

})


// card Items  ->  for lazying loading assingment

document.addEventListener('DOMContentLoaded', fetchProducts)


async function fetchProducts(url) {
    let productsContainers = document.querySelectorAll('.Products');
    let data = await fetch(url);
    let response = await data.json()

    console.log(response)

    for (let i = 0; i < 17; i++) {
        let description = response[i].description;
        let productHTML = `
                <div class="col mb-4">
                    <div class="card" style="box-shadow:2px 2px 10px 2px">
                        <img src=${response[i].images[1]} class="card-img-top" alt="...">
                        <div class="card-body">
                        <h5 class="card-title">${response[i].title}</h5>
                        <h6 class="card-title">${response[i].category.name}</h6>  
                        <p class='description'>${description.length > 20 ? description.substring(0, 20).concat('....') : description}</p>
                        <div class="price">
                        <h6 class="card-title">${response[i].price} Rs</h6>                            
                        <a href=''#' data-productId="${response[i].id}" class="add-card-id"></a>  
                        </div>                          
                        </div>
                    </div>
                </div>
            `;

        // Append the product card to each products container
        productsContainers.forEach(container => {
            container.innerHTML += productHTML;
        });
    }
};

window.onscroll = () => {
    const { scrollTop, clientHeight, scrollHeight } = document.documentElement;
    if (scrollTop + clientHeight + 1 >= scrollHeight) {
        loading.classList.add('show')
        fetchProducts('https://api.escuelajs.co/api/v1/products');

    }
    else {
        loading.classList.remove('show')
    }
}





